const express = require('express');

const app = express();
const PORT = 8050;

app.get("/api/test", (req, res) => {
    res.status(200).send({ message: 'GG'});
});

app.get("/api/saussure", (req, res) => {
    res.status(200).send({ message: 'Saussure'});
});
 
app.listen(PORT, () => {
    console.log(`Listening on http://localhost:${PORT}`);
});